import React from "react";
import "./App.css";
import Header from "./components/Header/Header";
import Main from "./components/Header/Main/Main";
import BookItem from "./components/Header/BookItem/BookItem";
// import { Route } from 'react-router';
// import { BrowserRouter } from 'react-router-dom';
import { BrowserRouter, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
            <header className="App-header">
                <Header />
          </header>
        {/* <Main /> */}
        {/* <BookItem /> */}
        <div className="App-wrap">
          <Route exact path="/" component={Main} />
          <Route path="/BookItem" component={BookItem} />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
