import React from "react";
import style from './BookItem.module.css'

const BookItem=()=>{
    return(
        <div className={style.BookContainer}>
            <div className={style.Image}>
                <img alt='solzhenitsyn' src="https://img4.labirint.ru/rc/090f4a88c2e5fc474f51622d66f15ab4/220x340/books21/208366/cover.png?1575286141"></img>
            </div>
            <div className={style.About}>
                <p>Art/General</p>
                <h2>GULAG </h2>
                <a href="https://www.labirint.ru/books/208366/">Solzhenitsyn A.I</a>
            </div>
            <div className={style.BorderAbout}>
                <h3>Books about politic of repressions</h3>
            </div>
        </div>
    )
}
export default BookItem;